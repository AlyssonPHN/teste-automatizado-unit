package br.com.alura.leilao.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.alura.leilao.exception.LanceMenorQueUltimoLanceException;
import br.com.alura.leilao.exception.LanceSeguidoDoMesmoUsuarioException;
import br.com.alura.leilao.exception.UsuarioJaDeuCincoLancesExceptions;

public class Leilao implements Serializable {

    private final String descricao;
    private final List<Lance> lances;
    private double maiorLance = 0.0;
    private double menorLance = 0.0;

    public Leilao(String descricao) {
        this.descricao = descricao;
        this.lances = new ArrayList<>();
    }

    public void prope(Lance lance) {
        verificaLanceNaoValido(lance);
        lances.add(lance);
        double valorLance = lance.getValor();
        if (verificaMaiorMenorLanceParaPrimeiroLance(valorLance)) return;

        Collections.sort(lances);
        calcularLance(valorLance);
    }

    private boolean verificaMaiorMenorLanceParaPrimeiroLance(double valorLance) {
        if (lances.size() == 1) {
            maiorLance = valorLance;
            menorLance = valorLance;
            return true;
        }
        return false;
    }

    private void verificaLanceNaoValido(Lance lance) {
        double valorLance = lance.getValor();
        if (lanceMenorQueUltimoLance(valorLance))
            throw new LanceMenorQueUltimoLanceException();
        if (temLances()) {
            Usuario usuarioNovo = lance.getUsuario();
            if (usuarioMesmodoUltimoLance(usuarioNovo))
                throw new LanceSeguidoDoMesmoUsuarioException();
            if (verificaUsuarioDeu5Lances(usuarioNovo))
                throw new UsuarioJaDeuCincoLancesExceptions();
        }
    }

    private boolean temLances() {
        return !lances.isEmpty();
    }

    private boolean verificaUsuarioDeu5Lances(Usuario usuarioNovo) {
        int lancesDoUsuario = 0;
        for (Lance flance : lances) {
            Usuario usuarioExistente = flance.getUsuario();
            if (usuarioExistente.equals(usuarioNovo)) {
                lancesDoUsuario++;
                if (lancesDoUsuario == 5) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean usuarioMesmodoUltimoLance(Usuario usuarioNovo) {
        Usuario ultimoUsuario = lances.get(0).getUsuario();
        if (usuarioNovo.equals(ultimoUsuario)) {
            return true;
        }
        return false;
    }

    private boolean lanceMenorQueUltimoLance(double valorLance) {
        if (maiorLance > valorLance) {
            return true;
        }
        return false;
    }

    private void calculaMenorLance(double valorLance) {
        if (valorLance < menorLance) {
            menorLance = valorLance;
        }
    }

    private void calcularLance(double valorLance) {
        if (valorLance > maiorLance) {
            maiorLance = valorLance;
        }
    }

    public String getDescricao() {
        return descricao;
    }

    public double getMaiorLance() {
        return maiorLance;
    }

    public double getMenorLance() {
        return menorLance;
    }

    public List<Lance> treMaioresLance() {
        int quantidadeMaximaLances = lances.size();
        if (quantidadeMaximaLances > 3) {
            quantidadeMaximaLances = 3;
        }
        return lances.subList(0, quantidadeMaximaLances);
    }

    public int quantidadedeLances() {
        return lances.size();
    }
}
