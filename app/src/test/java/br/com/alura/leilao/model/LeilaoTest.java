package br.com.alura.leilao.model;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.List;

import br.com.alura.leilao.exception.LanceMenorQueUltimoLanceException;
import br.com.alura.leilao.exception.LanceSeguidoDoMesmoUsuarioException;
import br.com.alura.leilao.exception.UsuarioJaDeuCincoLancesExceptions;

import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class LeilaoTest {

    public static final double DELTA = 0.0001;
    // criar cenário de teste
    private final Leilao console = new Leilao("Console");
    private final Usuario alex = new Usuario("Alex");



    @Test
    public void deve_DevolveDescricao_QuandoRecebeDescricao() {

        // executar acao necessária
        String descricaoDevolvida = console.getDescricao();
        // testar resultado esperado
//        assertEquals("Console", descricaoDevolvida);
//        assertThat(descricaoDevolvida, equalTo("Console"));
//        assertThat(descricaoDevolvida, is(equalTo("Console")));
        assertThat(descricaoDevolvida, is("Console"));
    }

    @Test
    public void deve_DevolvendoMaiorLance_QuandoRecebeApenasUmLance() {
        //Arrange
        console.prope(new Lance(alex, 200.0));
        //Act
        double maiorLanceDevolvidoConsole = console.getMaiorLance();
        //Assert
        // delta é usado entre double pois a double pode dar valor quebrado,
        // então na margem de erro de delta, ele aceita
        //assertEquals(200.0, maiorLanceDevolvidoConsole, DELTA);
        assertThat(maiorLanceDevolvidoConsole, closeTo(200.0, DELTA));
        // Para teste com números quebrados
        //assertThat(4.1 + 5.3, closeTo(4.4 + 5.0, DELTA));
    }

    @Test
    public void deve_DevolveMaiorLance_QuandoRecebeMaisDeUmLance_EmOrdemCrescente() {
        // Arrange
        console.prope(new Lance(alex, 100.0));
        console.prope(new Lance(new Usuario("Fran"), 200.0));
        // Act
        double maiorLanceDevolvidoComputador = console.getMaiorLance();
        //Assert
        assertEquals(200.0, maiorLanceDevolvidoComputador, DELTA);
    }

    @Test
    public void getMaiorLanceLance_QuandoRecebeMaisDeUmLanceEmOrdem_Decrescente_DevolveMenorLance() {
        // Arrange
        console.prope(new Lance(alex, 1000.0));
        try {
            console.prope(new Lance(new Usuario("Fran"), 200.0));
            fail("Espera um RuntimeException");
        } catch (RuntimeException ignored) {

        }
//        // Act
//        double maiorLanceDevolvidoCarro= console.getMaiorLance();
//        //Assert
//        assertEquals(1000.0, maiorLanceDevolvidoCarro, DELTA);
    }

    @Test
    public void deve_DevolvendoMenorLance_QuandoRecebeApenasUmLance() {
        //Arrange
        console.prope(new Lance(alex, 200.0));
        //Act
        double menorLance = console.getMenorLance();
        //Assert
        // delta é usado entre double pois a double pode dar valor quebrado,
        // então na margem de erro de delta, ele aceita
        assertEquals(200.0, menorLance, DELTA);
    }

    @Test
    public void deve_DevolveMenorLance_QuandoRecebeMaisDeUmLance_EmOrdemCrescente() {
        // Arrange
        console.prope(new Lance(alex, 100.0));
        console.prope(new Lance(new Usuario("Fran"), 200.0));
        // Act
        double menorLance = console.getMenorLance();
        //Assert
        assertEquals(100.0, menorLance, DELTA);
    }

    @Test
    public void deve_devolverTresMaioresLances_QuandoRecebeExatosTresLances() {
        // Arrange
        console.prope(new Lance(alex, 200.00));
        console.prope(new Lance(new Usuario("Fran"), 300.00));
        console.prope(new Lance(alex, 400.00));
        // Act
        List<Lance> tresMaioresLancesDevolvidos = console.treMaioresLance();
        // Assert
//        assertEquals(3, tresMaioresLancesDevolvidos.size());
//        assertThat(tresMaioresLancesDevolvidos, hasSize(3));
//        assertEquals(400.0,
//                tresMaioresLancesDevolvidos.get(0).getValor(), DELTA);
        //assertThat(tresMaioresLancesDevolvidos, hasItem(new Lance(alex, 400.00)));
//        assertEquals(300.0,
//                tresMaioresLancesDevolvidos.get(1).getValor(), DELTA);
//        assertEquals(200.0,
//                tresMaioresLancesDevolvidos.get(2).getValor(), DELTA);

//        assertThat(tresMaioresLancesDevolvidos, contains(
//                new Lance(alex, 400.00),
//                new Lance(new Usuario("Fran"), 300.00),
//                new Lance(alex, 200.00)
//                ));
        assertThat(tresMaioresLancesDevolvidos,
                both(Matchers.<Lance>hasSize(3))
                        .and(contains(
                                new Lance(alex, 400.00),
                                new Lance(new Usuario("Fran"), 300.00),
                                new Lance(alex, 200.00))));
    }

    @Test
    public void deve_devolverTresMaioresLances_QuandoNaoRecebeLances() {
        // Act
        List<Lance> tresMaioresLancesDevolvidos = console.treMaioresLance();
        // Assert
        assertEquals(0, tresMaioresLancesDevolvidos.size());
    }

    @Test
    public void deve_DevolverTresMaioresLances_QuandoRecebeApenasUmLance() {
        // Arrange
        console.prope(new Lance(alex, 200.0));
        // Act
        List<Lance> treMaioresLance = console.treMaioresLance();
        // Assert
        assertEquals(1, treMaioresLance.size());
        // Ver se o maior valor é 200
        assertEquals(200.0, treMaioresLance.get(0).getValor(), DELTA);
    }

    @Test
    public void deve_DevolverTresMaioresLances_QuandoRecebeApenasDoisLances() {
        // Arrange
        console.prope(new Lance(alex, 300.0));
        console.prope(new Lance(new Usuario("Fran"), 400.0));
        // Act
        List<Lance> treMaioresLance = console.treMaioresLance();
        // Assert
        assertEquals(2, treMaioresLance.size());
        assertEquals(400.0, treMaioresLance.get(0).getValor(), DELTA);
        assertEquals(300.0, treMaioresLance.get(1).getValor(), DELTA);
    }

    @Test
    public void deve_DevolverTresMaioresLances_QuandoRecebeMaisdeTresLances() {
        // Arrange
        final Usuario fran = new Usuario("Fran");
        console.prope(new Lance(alex, 300.0));
        console.prope(new Lance(fran, 400.0));
        console.prope(new Lance(alex, 500.0));
        console.prope(new Lance(fran, 600.0));
        // Act
        List<Lance> treMaioresLance = console.treMaioresLance();
        // Assert
        assertEquals(3, treMaioresLance.size());
        assertEquals(600.0, treMaioresLance.get(0).getValor(), DELTA);
        assertEquals(500.0, treMaioresLance.get(1).getValor(), DELTA);
        assertEquals(400.0, treMaioresLance.get(2).getValor(), DELTA);
    }

    @Test
    public void deve_devolverValorZeroParaMaiorLance_QuandoNaoTiverLances() {
        double maiorLance = console.getMaiorLance();
        assertEquals(0.0, maiorLance, DELTA);
    }

    @Test
    public void deve_DevolverValorZeroParaMenorLance_QuandoNaoTiverLances() {
        double menorLance = console.getMenorLance();
        assertEquals(0.0, menorLance, DELTA);
    }

    @Test(expected = LanceMenorQueUltimoLanceException.class)
    public void naoDeve_AdicionarLance_QuandoForMenorQueMaiorLance() {
        // Arrange
        console.prope(new Lance(alex, 500.0));
        console.prope(new Lance(new Usuario("Fran"), 400.0));
    }

    @Test(expected = LanceSeguidoDoMesmoUsuarioException.class)
    public void naoDeve_AdicionarLance_QuandoForOMesmoUsuarioDoUltimoLance() {
        //Arrange
        console.prope(new Lance(alex, 500.0));
        console.prope(new Lance(alex, 600.0));

    }

    @Test(expected = UsuarioJaDeuCincoLancesExceptions.class)
    public void naoDeve_AdicionarLance_QuandoUsuarioDerCincoLances() {
        // Arrange
        console.prope(new Lance(alex, 100.0));
        final Usuario fran = new Usuario("Fran");
        console.prope(new Lance(fran, 200.0));
        console.prope(new Lance(alex, 300.0));
        console.prope(new Lance(fran, 400.0));
        console.prope(new Lance(alex, 500.0));
        console.prope(new Lance(fran, 600.0));
        console.prope(new Lance(alex, 700.0));
        console.prope(new Lance(fran, 800.0));
        console.prope(new Lance(alex, 900.0));
        console.prope(new Lance(fran, 1000.0));
        console.prope(new Lance(alex, 1100.0));

    }
}